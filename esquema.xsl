<?xml version="1.0" encoding="UTF-16"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <link rel="stylesheet" type="text/css" href="style.css"/>
        <html>
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <body>
                <header>
                    <div><img src="imagen1.jpeg" alt=""></img></div>
                    <div><img src="imagen2.png" alt=""></img></div>
                </header>

                <nav> 
                    <ul>
                        <a> Juan Alfredo Garcia y Rubén Gallego</a>
                    </ul>
                </nav>


                <div style="float: left;">

                    <div id="left-column" class="column">
                        <h2>Resumen</h2>
                        <ul>
                            <p>Yahoo! Answers es un sitio web en el que los usuarios proponen cuestiones a la comunidad que son contestadas 
                            por los miembros de la misma.
                            </p>
                            <p>Este es el resultado del filtrado y discretización de los datos obtenidos de FullOct2007
                            </p>
                        </ul>
                    </div>
                    <div class="container">
                        <table  style="width:100%" class="tabla" border="1">
                                <tr bgcolor="#b3b3ff">
                                    <th class="row-1 columna_pregunta"> Pregunta </th>
                                    <th class="row-2 columna_tema"> Tema </th>
                                    <th class="row-3 columna_respuesta"> Mejor respuesta </th>
                                </tr>

                                <xsl:for-each select="ystfeed/vespaadd">
                                    <tr>
                                        <td><xsl:value-of select="document/subject"/></td>
                                        <td><xsl:value-of select="document/subcat"/></td>
                                        <td><xsl:value-of select="document/bestanswer"/></td>
                                    </tr>
                                </xsl:for-each>
                        </table>

                        <footer>
                            <p>Yahoo Answers. Práctica Bases de Datos Avanzadas. Trabajo XML</p>
                        </footer>
                    </div>
                </div>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>