import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Filtro {
	
	//Medida de los tiempos
    static long time_start = 0;
	static long time_end = 0;


    private static void startTimeCount() {
        time_start = System.currentTimeMillis();
    }

    private static long stopTimeCount() {
        time_end = System.currentTimeMillis();
        return time_end - time_start;
    }


	public static void main(String[] args) {
		cargarDatos();
	}

	private static void cargarDatos() {

		// /Inicializacion de variables de acceso a fichero
		File ini;
		FileReader fr;
		BufferedReader br;

		FileWriter fw = null;		
		PrintWriter pw = null;

		//Axuliares
		String aux = "";
		String post="";
//		String mainccat = "Juegos y Aficiones";
		String mainccat = "Arte y Humanidades";

		boolean almacena = false;
		boolean hilo_encontrado = false;
		float contador = 0, limite; 
		
		if(mainccat.equals("Juegos y Aficiones")) limite = 1547;
		else limite = 12236;

		// Acceso a fichero
		try {
			ini = new File("FullOct2007.xml");
			fr = new FileReader(ini);
			br = new BufferedReader(fr);
			Scanner fichero = new Scanner(br);

			fw = new FileWriter(mainccat+".xml");
			pw = new PrintWriter(fw);
			 	
			startTimeCount();
			
			pw.println("<?xml version='1.0' encoding='UTF-8'?>\n<ystfeed>");

			while (fichero.hasNextLine() && contador!=limite) {
				aux = fichero.nextLine();
				post+=aux+"\n";
				if(aux.startsWith("<vespaadd>")){
					post=aux+"\n";
					hilo_encontrado=true;
				}
				else if(aux.equals("</document></vespaadd>")&& almacena){
					hilo_encontrado = false;
					pw.print(post);
					almacena=false;
					contador++;
					continue;
				}
				if(hilo_encontrado && aux.startsWith("<maincat>"+mainccat))
					almacena=true;
			}
			
			pw.print("</ystfeed>");
			
			pw.close ();
			System.out.println("Fin !: "+stopTimeCount()+" ms");
		}

		catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		catch (Exception e1) {
			e1.printStackTrace();
		}

	}
}
